'use strict';

const service = require('../server/service');
const http = require('http');

//server initiation

const server = http.createServer(service);
server.listen(3000);

//check server status
server.on('listening', function() {
  console.log(`SLACK-API is listening on ${server.address().port} in ${service.get('env')}`);
})
